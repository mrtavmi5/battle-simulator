import { Unit } from "./Unit.js";

Unit.units = ["Alpha", "Bravo", "Charlie", "Delta", "Echo"].map(
  (name) => new Unit(name)
);
