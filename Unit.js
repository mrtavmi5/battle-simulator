const CRITICAL_FACTOR = 2;

export class Unit {
  static units = [];

  name = "";
  health = 100;

  constructor(name) {
    this.name = name;
    setTimeout(() => this.attack(), this.rechargeTime);
  }

  get rechargeTime() {
    return (1000 * this.health) / 100;
  }

  get damage() {
    return this.health / 100;
  }

  get criticalChance() {
    return 10 - this.health / 10;
  }

  attack() {
    if (this.health > 0) {
      const liveUnits = Unit.units.filter((u) => u.health > 0 && this != u);
      if (liveUnits.length === 0) {
        console.log(this.name + " IS WINNER!!!");
        return;
      }
      const attacked = Math.floor(Math.random() * liveUnits.length);
      const unit = liveUnits[attacked];
      const critNum = Math.random() * 100;
      const isCritical = critNum <= this.criticalChance;
      const damage = isCritical ? this.damage * CRITICAL_FACTOR : this.damage;
      console.log(
        `${this.name} (${this.health} hp) attacks ${unit.name} (${
          unit.health
        } hp) with ${isCritical ? "CRITICAL " : ""}damage ${damage}`
      );
      unit.health -= damage;
      if (unit.health <= 0) {
        console.log(`${this.name} KILLED ${unit.name}`);
      }

      setTimeout(() => this.attack(), this.rechargeTime);
    }
  }
}
